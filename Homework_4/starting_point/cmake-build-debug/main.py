#!/bin/env python3

import sys
import argparse
import pypart
from pypart import MaterialPointsFactory, ParticlesFactoryInterface
from pypart import PingPongBallsFactory, PlanetsFactory
from pypart import CsvWriter
from pypart import ComputeTemperature
from pypart import ComputeGravity
from pypart import ComputeVerletIntegration
# help(pypart)
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def main(nsteps,freq,filename,particle_type,timestep):
    """
    Function that creates particles simulation
    ========== Parameters ==========
    nsteps : an integer
    Describes the number of steps
    freq : an integer
    Describes frequency of dumping the data
    filename : a string
    Describes name of the file containing initial particles data
    particle_type : a string
    Describes particle type
    timestep : a float
    Describes the timestep for the simulation (in days)
    ========== Returns ==========
    
    """
    #print(pypart.__file__)

    if particle_type == "planet":
        PlanetsFactory.getInstance()
    elif particle_type == "ping_pong":
        PingPongBallsFactory.getInstance()
    elif particle_type == "material_point":
        MaterialPointsFactory.getInstance()
    else:
        print("Unknown particle type: ", particle_type)
        sys.exit(-1)

    factory = ParticlesFactoryInterface.getInstance()

    def createComputes(self, timestep):
        """
        Function that would be input to createSimulation method of factory
        ========== Parameters ==========
        timestep : a float
        Describes the timestep for the simulation (in days)
        ========== Returns ==========
        None
        """

        if particle_type == "planet":
    
            try:
                compute_grav = ComputeGravity()
                compute_verlet = ComputeVerletIntegration(timestep)
        
                G = 6.67384e-11 # m^3 * kg^-1 * s^-2
                UA = 149597870.700 # km
                earth_mass = 5.97219e24 # kg
                G /= (UA * 1e3)**3 # UA^3 * kg^-1 * s^-2
                G *= earth_mass    # UA^3 * earth_mass^-1 * s^-2
                G *= (60*60*24)**2 # UA^3 * earth_mass^-1 * day^-2
        
                compute_grav.setG(G)
                compute_verlet.addInteraction(compute_grav)
                self.system_evolution.addCompute(compute_verlet)
            
            except Exception as e:
                help(compute_grav)
                raise e
        
        elif particle_type == 'material_point':
        
            try:
                compute_temp = ComputeTemperature()
                compute_temp.conductivity = 1
                compute_temp.L = 2
                compute_temp.capacity = 1
                compute_temp.density = 1
                compute_temp.deltat = 1
                self.system_evolution.addCompute(compute_temp)
            except Exception as e:
                help(compute_temp)
                raise e

    evol = factory.createSimulation(filename, timestep, createComputes)

    dumper = CsvWriter("out.csv")
    dumper.write(evol.getSystem())
    
    evol.setNSteps(nsteps)
    evol.setDumpFreq(freq)
    evol.evolve()


def readPositions(planet_name,directory):
    """
    Function that reads the trajectory of a given planet
    and makes a numpy array out of it
    ========== Parameters ==========
    planet_name : a string 
    Describes the name of the planet
    directory : a string
    Describes the location of the trajectories file
    
    ========== Returns ==========
    A numpy matrix of 365x3 size.
    The rows refer to the days of a year.
    The columns represent the three components of the planet position.
    """

    if directory == "trajectories/":
        filler = 4
    elif directory == "dumps/":
        filler = 5
    trajectory = np.zeros((365,3))
    for days in range(0,365):
        file_name = str(days).zfill(filler)
        f = open(directory + "step-" + file_name + ".csv", "r")
        lines = f.readlines()
        f.close()
        for i in range(len(lines)):
            line = lines[i].split()
            if line[-1] == planet_name:
                pos = np.array([float(line[0]), float(line[1]), float(line[2])])
            
        trajectory[days,:] = pos
    return trajectory

def computeError(positions,positions_ref):
    """
    Function that computes the integral error between 
    the reference position of a planet and its computed position
    ========== Parameters ==========
    positions : a string
    Describes the location of the planets' computed positions
    positions_ref : a string
    Describes the location of the planets' true positions  
    ========== Returns ==========
    A float which represents to error in positions of given particle
    """

    diff = positions - positions_ref
    error = np.sum(np.linalg.norm(diff,axis=1))**0.5
    
    return error


def generateInput(scale,planet_name,input_filename,output_filename):
    """
    Function that generates an input file from a given input file
    by scaling velocity of a given planet
    ========== Parameters ==========
    scale : a float
    Describes the scale factor
    planet_name : a string
    Describes the name of the planet
    input_filename : a string
    Describes the filename to read the planet velocities from
    output_filename : a string
    Descibes the filename where the updated velocities are stored
    ========== Returns ==========
    None
    """

     scale = scale[0]
     f = open(input_filename, "r")
     lines = f.readlines()
     f.close()
     f = open(output_filename, "w")
     seperator = " "
     for i in range(len(lines)):
         line = lines[i].split()
         if line[-1] == planet_name:
             line[3] = str(float(line[3])*scale)
             line[4] = str(float(line[4])*scale)
             line[5] = str(float(line[5])*scale)
         line.append("\n")
         f.write(seperator.join(line))
     f.close()  


def launchParticles(input,nb_steps,freq):
    """
    Function that launches the particle code on a provided input
    ========== Parameters ==========
    input : a string
    Describes a filename to read the initial planets' locations from
    nb_steps : an integer
    Describes the number of steps of the simulation
    freq : an integer
    Describes the frequency of printing
    ========== Returns ==========
    None
    """

    main(nb_steps,freq,input,"planet",1)


def runAndComputeError(scale,planet_name,input,nb_steps,freq):
    """
    Function that gives the error for a given scaling velocity factor of a given planet
    ========== Parameters ==========
    scale : a float
    Describes the scale factor
    planet_name : a string
    Describes the name of a planet
    input : a string
    Describes the filename where to load the planet locations from
    nb_steps : an integer
    Describes the number of steps of simulation
    freq: an integer
    Describes the frequency of printing
    ========== Returns ==========
    A float which represents to error in positions of given particle
    """

    generateInput(scale, planet_name, input, "scaled_init.csv")
    launchParticles("scaled_init.csv", nb_steps, freq)
    positions = readPositions(planet_name, "dumps/")
    os.chdir("..")
    positions_ref = readPositions(planet_name, "trajectories/")
    os.chdir("cmake-build-debug")
    
    return computeError(positions, positions_ref)

def runMinimizer(guess):
    """
    Function that plots the evolution of the error versus the scaling factor
    and finds the initial velocity for Mercury. Plots the graph of error evolution
    ========== Parameters ==========
    guess : a numpy array
    Describes the initial guess for the iterative minimization process.
    ========== Returns ==========
    None
    """

    x_progress = [np.array([guess])]
    func_error = []
    def store(x):
        x_progress.append(x)
    
    minimize(runAndComputeError, guess, args=("mercury", "init.csv", 365, 1),
                   method='Nelder-Mead', tol=1e-6, callback=store)
    
    
    for i in range(len(x_progress)):
        func_error.append(runAndComputeError(x_progress[i], "mercury",  "init.csv", 365, 1))
    
    plt.plot(x_progress, func_error, marker='x', c="k", zorder=1)
    plt.scatter(x_progress[-1], func_error[-1], marker="x", c='r', s=100, zorder=2, 
                label="minimum scaling %.2f w/ error %.2f UA/day" %(x_progress[-1], func_error[-1]))
    plt.xlabel("Scaling Factor")
    plt.ylabel("Error (UA/day)")
    plt.legend()
    plt.savefig("Error_plot.png")



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Particles code')
    parser.add_argument('nsteps', type=int,
                        help='specify the number of steps to perform')
    parser.add_argument('freq', type=int,
                        help='specify the frequency for dumps')
    parser.add_argument('filename', type=str,
                        help='start/input filename')
    parser.add_argument('particle_type', type=str,
                        help='particle type')
    parser.add_argument('timestep', type=float,
                        help='timestep')
    
    args = parser.parse_args()
    nsteps = args.nsteps
    freq = args.freq
    filename = args.filename
    particle_type = args.particle_type
    timestep = args.timestep

    main(nsteps,freq,filename,particle_type,timestep)
    if particle_type == "planet":
        runMinimizer(1)














#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  // bind the routines here

  py::class_<ParticlesFactoryInterface, std::shared_ptr<ParticlesFactoryInterface>>(m, "ParticlesFactoryInterface")
      .def("getInstance", &ParticlesFactoryInterface::getInstance,py::return_value_policy::reference)
      .def("createSimulation",py::overload_cast<const std::string &, Real, py::function>
               (&ParticlesFactoryInterface::createSimulation<py::function>),py::return_value_policy::reference)
      .def("getSystemEvolution", &ParticlesFactoryInterface::getSystemEvolution, py::return_value_policy::reference)
      .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);


  py::class_<PlanetsFactory, ParticlesFactoryInterface, std::shared_ptr<PlanetsFactory>>(m, "PlanetsFactory")
     .def("getInstance", &PlanetsFactory::getInstance,py::return_value_policy::reference)
     .def("createSimulation",py::overload_cast<const std::string &, Real, py::function>(
                 &ParticlesFactoryInterface::createSimulation<py::function>),py::return_value_policy::reference);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface, std::shared_ptr<MaterialPointsFactory>>(m, "MaterialPointsFactory")
      .def("getInstance", &MaterialPointsFactory::getInstance,py::return_value_policy::reference)
      .def("createSimulation", (SystemEvolution & (MaterialPointsFactory::*)(const std::string&, Real,
                  py::function))(&MaterialPointsFactory::createSimulation), py::return_value_policy::reference)
      .def("createSimulation", (SystemEvolution & (MaterialPointsFactory::*)(const std::string&, Real))(&MaterialPointsFactory::createSimulation),
               py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface, std::shared_ptr<PingPongBallsFactory>>(m, "PingPongBallsFactory")
      .def("getInstance", &PingPongBallsFactory::getInstance,py::return_value_policy::reference)
      .def("createSimulation", (SystemEvolution & (PingPongBallsFactory::*)(const std::string&, Real,
                  py::function))(&PingPongBallsFactory::createSimulation), py::return_value_policy::reference)
      .def("createSimulation", (SystemEvolution & (PingPongBallsFactory::*)(const std::string&, Real))(&PingPongBallsFactory::createSimulation),
               py::return_value_policy::reference);

  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
    .def(py::init<>(), py::return_value_policy::reference)
    .def("compute", &ComputeGravity::compute, py::return_value_policy::reference)
    .def("setG", &ComputeGravity::setG, py::return_value_policy::reference)
    .def("getG", &ComputeGravity::getG, py::return_value_policy::reference)
    //.def_readwrite("G", &ComputeGravity::G, py::return_value_policy::reference); // give read write access (property) to member
    .def_property("G", &ComputeGravity::getG, &ComputeGravity::setG);

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
    .def(py::init<>(), py::return_value_policy::reference)
    .def("compute", &ComputeTemperature::compute, py::return_value_policy::reference)
    .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::setConductivity)
    .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::setCapacity)
    .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::setDensity)
    .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
    .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::setDeltat);


    py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
    .def(py::init<const Real &>(), py::return_value_policy::reference)
    .def("compute", &ComputeVerletIntegration::compute, py::return_value_policy::reference)
    .def("addInteraction", &ComputeVerletIntegration::addInteraction, py::return_value_policy::reference)
    .def("setDeltaT", &ComputeVerletIntegration::setDeltaT, py::return_value_policy::reference);

  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
     .def(py::init<const std::string &>())
     .def("write", &CsvWriter::write, py::return_value_policy::reference)
     .def("compute", &CsvWriter::compute, py::return_value_policy::reference);

  py::class_<SystemEvolution/*, std::shared_ptr<SystemEvolution>*/>(m, "SystemEvolution")
            //.def(py::init<std::unique_ptr<System>>(), py::return_value_policy::reference)
     .def("evolve", &SystemEvolution::evolve, py::return_value_policy::reference)
     .def("addCompute", &SystemEvolution::addCompute, py::return_value_policy::reference)
     .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
     .def("setNSteps", &SystemEvolution::setNSteps, py::return_value_policy::reference)
     .def("setDumpFreq", &SystemEvolution::setDumpFreq, py::return_value_policy::reference);

    py::class_<System, std::shared_ptr<System>>(m, "System")
     .def(py::init<>(), py::return_value_policy::reference)
     .def("getParticle", &System::getParticle, py::return_value_policy::reference)
     .def("addParticle", &System::addParticle, py::return_value_policy::reference)
     .def("removeParticle", &System::removeParticle, py::return_value_policy::reference)
     .def("getNbParticles", &System::getNbParticles, py::return_value_policy::reference);

    py::class_<Particle>(m, "Particle")
     .def(py::init<>(), py::return_value_policy::reference)
     .def("getMass", &Particle::getMass, py::return_value_policy::reference)
     .def("getPosition", &Particle::getPosition, py::return_value_policy::reference)
     .def("getForce", &Particle::getForce, py::return_value_policy::reference)
     .def("getVelocity", &Particle::getVelocity, py::return_value_policy::reference)
     .def_property("mass", &Particle::getMass, nullptr)
     .def_property("position", &Particle::getPosition, nullptr)
     .def_property("force", &Particle::getForce, nullptr)
     .def_property("velocity", &Particle::getVelocity, nullptr);

    py::class_<Planet, Particle>(m, "Planet")
      .def(py::init<>(), py::return_value_policy::reference)
      .def("initself", &Planet::initself, py::return_value_policy::reference)
      .def("printself", &Planet::printself, py::return_value_policy::reference)
      .def("getName", &Planet::getName, py::return_value_policy::reference)
      .def_property("getName", &Planet::getName, nullptr);

}



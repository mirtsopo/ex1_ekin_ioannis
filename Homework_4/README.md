**Exercise 4 - Scientific Programming for Engineers**
 
------------------------------------------------------
Description
------------------------------------------------------
This repository contains families of classes written in C++. The goal of this assignment is to use the external library Pybind11 to create Python bindings of the C++ Particle’s code, which is already provided.
 
------------------------------------------------------
Requirements
------------------------------------------------------
The code is tested with the following versions
- Python 3.8.3
- Argparse 1.1
- C++17
- Clion 2020.2.3
- Pybind11 2.6.1 (https://github.com/pybind/pybind11.git)
 
------------------------------------------------------
Instructions
------------------------------------------------------
To obtain the repository run the code below on terminal:
 
`git clone git@gitlab.epfl.ch:mirtsopo/ex1_ekin_ioannis.git` // clone the repo
 
Outside the main directory you can find the following files:
- **README.md** : File that contains the necessary documentation
 
The main directory of the repository should include the following files and folders:
 
**starting_point**   (folder): Contains the following .hh and .cc files:
 
--**cmake-build-debug**   (folder): Includes googletest library, pybind library, the compiled python module (.so), the init file and the main.py needed for Part II.
 
--**googletest**  (folder): Googletest files
 
--**eigen**  (folder): Eigen library files
 
--**pybind11**  (folder): Pybind11 library files
 
--**trajectories** (folder): Includes the .csv files that define the true planet positions in time.
 
- **compute.hh**: Base class for all compute
 
- **compute_boundary.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for computing the system interaction with the boundary.
 
- **compute_boundary.hh**:  A child class of “Compute”; takes care of computing the interactions with the simulation box. The file contains the constructor, the method (“compute”) that needs to be implemented and names the class members.
 
- **compute_contact.cc**: The file contains the implementations of the accessors and the methods defined in the respective .hh file. These implementations are responsible for computing the contact interactions between ping-pong balls.
 
- **compute_contact.hh**: A child class of “ComputeInteraction”; takes care of computing the contact interactions between ping-pong balls. The file contains the accessors (“setPenalty”) and the method (“compute”) that need to be implemented and names the class members.
 
- **compute_energy.cc**: Not implemented yet. There for future use. The implementation must follow what is already indicated in the respective .hh file.
 
- **compute_energy.hh**: A child class of “Compute”; base class for energy computation. The file contains: the methods (“getValue”) that need to be implemented and names the class members.
 
- **compute_gravity.cc**:  The file contains the implementations of the accessors and the  methods defined in the respective .hh file. These implementations are responsible for the Newton gravity implementation.
 
- **compute_gravity.hh**: A child class of “ComputeInteraction”; takes care of computing the Newton gravity interactions. The file contains the accessors (“setG”) and the method (“compute”) that need to be implemented and names the class members.
 
- **compute_interaction.cc**: Nothing is implemented yet. There for future use. The implementation must follow what is already indicated in the respective .hh file.
 
- **compute_interaction.hh**: A child class of “Compute”; base class for interaction computation. The file contains the implementation of a functor (“applyOnPairs”) which is called by compute_contact.cc and compute_gravity.cc.
 
- **compute_kinetic_energy.cc**: Not implemented yet. For future use.
 
- **compute_kinetic_energy.hh**: A child class of “ComputeEnergy”; takes care of computing the kinetic energy of the system. The file contains the method (“compute”) that need to be implemented.
 
- **compute_potential_energy.cc**: Not implemented yet. For future use.
 
- **compute_potential_energy.hh**: A child class of “ComputeEnergy”; takes care of computing the potential energy of the system. The file contains the constructor and the method (“compute”) that need to be implemented and names the class members.
 
- **compute_temperature.cc**: The file contains the implementations of the methods defined in the respective .hh file. Uses fftw3 library to solve the heat equation in fourier space. The timestep can be changed by using setDeltaT methods and the boundaries can be made periodic and set to zero temperature by setBoundaries.
 
- **compute_temperature.hh**: A child class of “Compute”; takes care of temperature evolution of a system consisting of materials points. The file contains the constructor and the methods (“compute”, “setDeltaT”, “setBoundaries”) that need to be implemented and names the class members.
 
- **compute_verlet_integration.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are used for particle types other than material_point.
 
- **compute_verlet_integration.hh**: A child class of “Compute”; takes care of integrating the equation of motion. The file contains the constructor and the methods (“compute”, “setDeltaT”, “addInteraction”) that need to be implemented and names the class members.
 
- **csv_reader.cc**: The file contains the implementations of the constructor and the  methods defined in the respective .hh file. These implementations are responsible for reading a system from a csv input file.
 
- **csv_reader.hh**: A child class of “Compute”; reads a system from a csv input file. The file contains the constructor and the methods that need to be implemented (“read”, “compute”) and names the class members.
 
- **csv_writer.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for writing a system state to a csv input file.
 
- **csv_writer.hh**: A child class of “Compute”; writes a system state to a csv input file. The file contains the constructor and the methods that need to be implemented (“read”, “compute”) and names the class members.
 
- **Doxyfile**: Generates the documentation. Run doxygen to obtain the html of the documentation. (requires doxygen to be install in the system)
 
- **fft.hh**: A struct definition that uses fftw3 library and matrix templated class (and relevant iterator classes) to take the forward and backwards fourier of a given matrix and provides 2D frequency matrix where each element is the sum of the squares of frequencies in each dimension.
 
- **material_point.cc**: The file contains the implementations of the functions and the accessors defined in the respective .hh file. These implementations are responsible for manipulating material points.
 
- **material_point.hh**: A child class of “Particle”; takes care of material points. The file contains the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getTemperature”, “getHearRate”) and names the class members.
 
- **material_points_factory.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for manipulating material point factories.
 
- **material_points_factory.hh**: A child interface class of “ParticlesFactoryInterface”; takes care of generating a system consisting of material points. The file contains the constructor and the methods that need to be implemented (“createSimulation”, “createParticle”, “getInstance”).
 
- **matrix.hh**: The file contains the template of a struct representing a matrix of a given type and helping methods for manipulating Matrix structs.
 
- **my_types.hh**: using declarations for variable types
 
- **particle.cc**: The file contains the implementations of the methods defined in the respective .hh file. These implementations are responsible for manipulating particle objects.
 
- **particle.hh**: The base class for Particle objects. The file contains the constructor, the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getMass”, “getPosition”, “getForce”, “getVelocity”) and names the class members.
 
- **particles_factory_interface.cc**: The file contains the implementations of the methods defined in the respective .hh file. These implementations are responsible for creating interface objects.
 
- **particles_factory_interface.hh**: Interface class for particles factory. It is the parent interface class for materials_points_factory. Inherits methods to create instances and simulation systems to materials_points_factory.
 
- **ping_pong_ball.cc**: The file contains the implementations of the functions and the accessors defined in the respective .hh file. These implementations are responsible for manipulating ping pong balls.
 
- **ping_pong_ball.hh**: A child class of “Particle”.The file contains the I/O functions and the accessors that need to be implemented  (“printself”, “initself”, “getContactDissipation”, “getRadius”).
 
- **ping_pong_balls_factory.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for manipulating ping pong ball factories.
 
- **ping_pong_balls_factory.hh**:  A child interface class of “ParticlesFactoryInterface”; takes care of generating a system consisting of ping-pong balls. The file contains the constructor and the methods that need to be implemented (“createSimulation”, “createParticle”, “getInstance”).
 
- **planet.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating planet objects.
 
- **planet.hh**: A child class of “Particle”. The file contains the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getName”).
 
- **planets_factory.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for creating the planet system evolution.
 
- **planets_factory.hh**: A child of “ParticlesFactoryInterface” that generates the planet particles.
 
- **system.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating planet objects.
 
- **system.hh**: Class definition that uses particle objects to create and manipulate systems to be simulated.
 
- **system_evolution.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating system objects.
 
- **system_evolution.hh**: Class definition that uses evolve system objects.
 
- **test_fft.cc**: Test for the fft implementation. There are 6 tests in total; 2 for the fourier and inverse fourier, 1 for the frequencies of the fourier transformation, 3 for test example cases.
 
- **test_kepler.cc**: Test for the planets implementation
 
Main files:
- **CMakeLists.txt**: cmake file that compiles pypart target (pypart.cc). It has links to the class definitions and libraries.
 
- **init.csv**: Initial coordinates of a planet system that is used to initialize a simulation
 
- **main.py** (inside “starting_point/cmake-build-debug”):Uses bind classes to create a particle system and simulates for the given number of steps. Also includes functions to compare the simulation results with true trajectories and optimizes simulation inputs to match true trajectories.
 
- **Error_plot.png**: Error plot generated by the minimizer which shows the error as a function of the scaling factor.
 
- **pypart.cc**:  This is the file that is responsible for all the bindings between the C++ classes and Python. Practically, the included code allows us to “import pypart” in the Python editor. This file has to be compiled.
 
------------------------------------------------------
Example Usage
------------------------------------------------------
 
The binding has already been done. The user is only required to run main.py. In order to do so; go to the cmake-build-debug directory and in the terminal type:

	python3 main.py 365 1 init.csv planet 1
 
This would create a planet system for 365 steps with a timestep of 1 day and create dump files in the directory /dumps at a frequency of 1. To observe the input variables in more detail, in the terminal type:
 
	python3 main.py -h
 
Note that the minimizer will only run if the particle type is “planet”. The frequency must be declared to be 1 to run the optimizer without error. The error plot generated by the minimizer will be in the same directory.

------------------------------------------------------
Questions to be Answered
------------------------------------------------------

Exercise 1.2 : The function  in overloaded createSimulation method in ParticlesFactoryInterface creates necessary computes according to the specified particle type and uses the previously created createSimulation method to create a simulation as usual.
 
Exercise 2.2 : The binding can’t be checked using debugger mode in a Python IDE (like Spyder) the usage of the function is binded with C++. The bindings were checked by writing some extra getter methods in some classes and running specific lines of pypart.py in the IDE console. 
 
------------------------------------------------------
Authors
------------------------------------------------------
- R. Ekin Kubilay
- Ioannis Mirtsopoulos
 


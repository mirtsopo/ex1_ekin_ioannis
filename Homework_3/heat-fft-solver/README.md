**Exercise 3 - Scientific Programming for Engineers**

------------------------------------------------------
Description
------------------------------------------------------
This repository contains families of classes written in C++. Their functionalities are framed around an efficient heat equation solver, following the user's preferences regarding the heat source configuration.

------------------------------------------------------
Requirements
------------------------------------------------------
The code is tested with the following versions
- Python 3.8.3
- Argparse 1.1
- C++17
- Clion 2020.2.3

------------------------------------------------------
Instructions
------------------------------------------------------
To obtain the repository run the code below on terminal:

`git clone git@gitlab.epfl.ch:mirtsopo/ex1_ekin_ioannis.git` // clone the repo

Outside the main directory you can find the following files:
- **temp_vid.avi**: animated visualization using Paraview
- **temp_data_visualisation.pvsm**: Paraview source file used to generate the visualization animation
- **README.md** : File that contains the necessary documentation

The main directory of the repository should include the following folders, files and folders:

**starting_:point**   (folder): Contains the following .hh and .cc files:

--**cmake-build-debug** 	(folder): Includes googletest files doxygen, initializer data files, and the dumps folder.

--**googletest** 	(folder): Googletest files

--**html** 	(folder): Doxygen generated documentation html files and figures.

- **compute.hh**: Base class for all compute

- **compute_boundary.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for computing the system interaction with the boundary.

- **compute_boundary.hh**:  A child class of “Compute”; takes care of computing the interactions with the simulation box. The file contains the constructor, the method (“compute”) that need to be implemented and names the class members.

- **compute_contact.cc**: The file contains the implementations of the accessors and the methods defined in the respective .hh file. These implementations are responsible for computing the contact interactions between ping-pong balls.

- **compute_contact.hh**: A child class of “ComputeInteraction”; takes care of computing the contact interactions between ping-pong balls. The file contains the accessors (“setPenalty”) and the method (“compute”) that need to be implemented and names the class members.

- **compute_energy.cc**: Not implemented yet. There for future use. The implementation must follow what is already indicated in the respective .hh file.

- **compute_energy.hh**: A child class of “Compute”; base class for energy computation. The file contains: the methods (“getValue”) that need to be implemented and names the class members.

- **compute_gravity.cc**:  The file contains the implementations of the accessors and the  methods defined in the respective .hh file. These implementations are responsible for the Newton gravity implementation.

- **compute_gravity.hh**: A child class of “ComputeInteraction”; takes care of computing the Newton gravity interactions. The file contains the accessors (“setG”) and the method (“compute”) that need to be implemented and names the class members.

- **compute_interaction.cc**: Nothing is implemented yet. There for future use. The implementation must follow what is already indicated in the respective .hh file.

- **compute_interaction.hh**: A child class of “Compute”; base class for interaction computation. The file contains the implementation of a functor (“applyOnPairs”) which is called by compute_contact.cc and compute_gravity.cc.

- **compute_kinetic_energy.cc**: Not implemented yet. For future use.

- **compute_kinetic_energy.hh**: A child class of “ComputeEnergy”; takes care of computing the kinetic energy of the system. The file contains the method (“compute”) that need to be implemented.

- **compute_potential_energy.cc**: Not implemented yet. For future use.

- **compute_potential_energy.hh**: A child class of “ComputeEnergy”; takes care of computing the potential energy of the system. The file contains the constructor and the method (“compute”) that need to be implemented and names the class members.

- **compute_temperature.cc**: The file contains the implementations of the methods defined in the respective .hh file. Uses fftw3 library to solve the heat equation in fourier space. The timestep can be changed by using setDeltaT methods and the boundaries can be made periodic and set to zero temperature by setBoundaries.

- **compute_temperature.hh**: A child class of “Compute”; takes care of temperature evolution of a system consisting of materials points. The file contains the constructor and the methods (“compute”, “setDeltaT”, “setBoundaries”) that need to be implemented and names the class members.

- **compute_verlet_integration.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are used for particle types other than material_point.

- **compute_verlet_integration.hh**: A child class of “Compute”; takes care of integrating the equation of motion. The file contains the constructor and the methods (“compute”, “setDeltaT”, “addInteraction”) that need to be implemented and names the class members.

- **csv_reader.cc**: The file contains the implementations of the constructor and the  methods defined in the respective .hh file. These implementations are responsible for reading a system from a csv input file.

- **csv_reader.hh**: A child class of “Compute”; reads a system from a csv input file. The file contains the constructor and the methods that need to be implemented (“read”, “compute”) and names the class members.

- **csv_writer.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for writing a system state to a csv input file.

- **csv_writer.hh**: A child class of “Compute”; writes a system state to a csv input file. The file contains the constructor and the methods that need to be implemented (“read”, “compute”) and names the class members.

- **Doxyfile**: Generates the documentation. Run doxygen to obtain the html of the documentation. (requires doxygen to be install in the system)

- **fft.hh**: A struct definition that uses fftw3 library and matrix templated class (and relevant iterator classes) to take the forward and backwards fourier of a given matrix and provides 2D frequency matrix where each element is the sum of the squares of frequencies in each dimension.

- **material_point.cc**: The file contains the implementations of the functions and the accessors defined in the respective .hh file. These implementations are responsible for manipulating material points.

- **material_point.hh**: A child class of “Particle”; takes care of material points. The file contains the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getTemperature”, “getHearRate”) and names the class members.

- **material_points_factory.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for manipulating material point factories.

- **material_points_factory.hh**: A child interface class of “ParticlesFactoryInterface”; takes care of generating a system consisting of material points. The file contains the constructor and the methods that need to be implemented (“createSimulation”, “createParticle”, “getInstance”).

- **matrix.hh**: The file contains the template of a struct representing a matrix of a given type and helping methods for manipulating Matrix structs.

- **my_types.hh**: using declarations for variable types

- **particle.cc**: The file contains the implementations of the methods defined in the respective .hh file. These implementations are responsible for manipulating particle objects.

- **particle.hh**: The base class for Particle objects. The file contains the constructor, the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getMass”, “getPosition”, “getForce”, “getVelocity”) and names the class members.

- **particles_factory_interface.cc**: The file contains the implementations of the methods defined in the respective .hh file. These implementations are responsible for creating interface objects.

- **particles_factory_interface.hh**: Interface class for particles factory. It is the parent interface class for materials_points_factory. Inherits methods to create instances and simulation systems to materials_points_factory.

- **ping_pong_ball.cc**: The file contains the implementations of the functions and the accessors defined in the respective .hh file. These implementations are responsible for manipulating ping pong balls.

- **ping_pong_ball.hh**: A child class of “Particle”.The file contains the I/O functions and the accessors that need to be implemented  (“printself”, “initself”, “getContactDissipation”, “getRadius”).

- **ping_pong_balls_factory.cc**: The file contains the implementations of the constructor and the methods defined in the respective .hh file. These implementations are responsible for manipulating ping pong ball factories.

- **ping_pong_balls_factory.hh**:  A child interface class of “ParticlesFactoryInterface”; takes care of generating a system consisting of ping-pong balls. The file contains the constructor and the methods that need to be implemented (“createSimulation”, “createParticle”, “getInstance”).

- **planet.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating planet objects.

- **planet.hh**: A child class of “Particle”. The file contains the I/O functions and the accessors that need to be implemented (“printself”, “initself”, “getName”).

- **system.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating planet objects.

- **system.hh**: Class definition that uses particle objects to create and manipulate systems to be simulated.

- **system_evolution.cc**: The file contains the implementations of the functions defined in the respective .hh file. The methods are responsible for manipulating system objects.


- **system_evolution.hh**: Class definition that uses evolve system objects.

- **test_fft.cc**: Test for the fft implementation. There are 6 tests in total; 2 for the fourier and inverse fourier, 1 for the frequencies of the fourier transformation, 3 for test example cases. The tested cases are as follows;
  * Zero initial temperature and no heat source.
  * Sinusoidal heat source with zero initial temperature field
  * Line heat sources at x=+/-0.5 and zero initial temperature field.
  The tests are run for a coarse discretization for efficiency. The convergence of the code depends on timestep size and number of steps. The results are compared with the analytical results. Failing tests imply non-convergence. Note that the frequency matrix is different from what numpy generates. In this implementation, the frequency matrix consists of the sum of the squares of pi times the frequencies corresponding at each fourier bin. 

- **test_kepler.cc**: Test for the planets implementation

Main files:
- **CMakeLists.txt**: cmake file that creates the “particles” executable, with necessary links to the class definitions and libraries.

- **main.cc**: Assigns the inputs to relevant variables and runs the simulation accordingly.

- **generate_input.py**: Python file used to generate input files with initial temperature and heat source values.

- **temp_data.py**: Python file that generates initialization files necessary for running the simulations. The code also provides files that represent the analytical solution of the given case. Requires 2 inputs as explained in the Example Usage section.


------------------------------------------------------
File overview
------------------------------------------------------
- **main.cc**: 6 arguments must be given in the order explained in the Instructions. Arguments are cast into relevant variables and necessary objects are created and necessary methods are called depending on the input. If the arguments don't match with the described options or incorrect number of arguments given, the code will exit without success. (see Example Usage section for more details).

- **generate_input.py**: Python file that generates initialization files necessary for running the simulations other than material_points.

------------------------------------------------------
Example Usage
------------------------------------------------------
In order to generate input files, run the python script, temp_data.py on a terminal as follows;
python3 temp_data.py N R.
N: the discretization. In the above example, 128x128 matrix would be created.
R (0<R<1): the parameter which defines the circular region in which heat is provided.
The files will be generated in the directory /cmake-build-debug.
Note: To run the simulation described in Exercise 4.4, make sure the point x=+/-0.5 is included in your discretization. Otherwise, the heat field would be zero.

In order to run the tests, compile test_fft.cc and run (either in clion or in a terminal). The contents are explained in Instructions section.

In order to run the simulation, compile the code and run (either in clion or in a terminal) with seven parameters with this order;
N freq init_file_name material_point timestep boundary_flag
N: Number of steps. Ideally, it should be large.
freq: frequency at which dump files will be generated. Files will be generated in the directory /cmake-build-debug/dumps.
init_file_name: name of the .csv file that has the inputs. The .csv inputs should be ordered such that data points should be written row by row starting from the top left and bottom right data point should be written last. First three values are x, y, z coordinates (z coordinate is not used so can be any value), v_x, v_y, v_z, f_x, f_y, f_z, m (velocity, force and mass, not used, could be anything), temperature and heat source. Python file used to generate the init files gives 0 or 1 to unused variables.
material_point: type of the class used. This should not be changed.
timestep: timestep used for the iterative scheme. Ideally, it should be small.
boundary_flag: 0 or 1 should be given. 0 creates periodic boundaries, 1 ensures that boundaries always have zero temperature. When zero temperature boundaries would be used, the method works as usual but before moving on to the next iteration, the temperature at the boundaries are altered to zero. The system still remains periodic since temperature is zero at all boundaries (thus the fourier space solution is still valid).

A sample input would be as follows;
8001 200 temp_data_q5_init.csv material_point 0.00185 1

Note: Increasing the number of points in the system requires the step size to be decreased to avoid divergence. Low step size requires a large number of steps to achieve results close to analytical solutions. Be cautious when changing these variables. Consider decreasing the timestep and increasing the number of steps N when running the simulation for a larger number of points.

To run the system described in Exercise 4.2, run
8001 200 temp_data_q2_init.csv material_point 0.00185 0

To run the system described in Exercise 4.3, run
8001 200 temp_data_q3_init.csv material_point 0.00185 0

To run the system described in Exercise 4.4, run
8001 200 temp_data_q4_init.csv material_point 0.00185 0

To run the system described in Exercise 4.5, run
8001 200 temp_data_q2_init.csv material_point 0.00185 1

It is possible to change the boundaries, dump frequency, number of steps, and timestep (latter two are suggested for efficiency). Number of points should be controlled via the temp_data.py file.

------------------------------------------------------
ParaView visualization
------------------------------------------------------
ParaView requires files data stored in .csv format in order to create animated visualizations. For the visualization of heat transfer from heat sources, .csv files stored in Homework_3\heat-fft-solver\starting_point\cmake-build-debug\dumps are used. The generated .csv files refer to a 64x64 grid of points. The generation of data for a 512x512 grid was too time consuming and a smaller grid was chosen. After loading the .csv to the software, the filter of “Table to Points” is used. There, the columns that refer to the particle coordinates (Field 0, Field 1, Field 2) are selected. Upon successfully converting the .csv files into a spreadsheet and translating the stored information into points, the renderview shows a grid of points. Adding an additional filter (“Glyph”), we can add dimensions to these points/particles. Afterwards, the 2D or 3D dimensional objects can be colored according to the temperature, which is stored in the .csv files along with the particles’ coordinates. The visualization options following are subject to the user preferences. A short animated visualization file has been included outside of the main folder of the implementation (temp_vid.avi) along with the file used to prepare it (temp_data_visualisation.pvsm).

------------------------------------------------------
Authors
------------------------------------------------------
- R. Ekin Kubilay
- Ioannis Mirtsopoulos

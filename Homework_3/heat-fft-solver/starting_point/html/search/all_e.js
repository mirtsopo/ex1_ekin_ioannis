var searchData=
[
  ['r_152',['R',['../namespacetemp__data.html#a1e0757f45b48124f42e20b4e594c5dc7',1,'temp_data']]],
  ['randomplanets_153',['RandomPlanets',['../class_random_planets.html',1,'']]],
  ['read_154',['read',['../class_csv_reader.html#a7d61ccdd184148d138932c1052f29cca',1,'CsvReader']]],
  ['real_155',['Real',['../my__types_8hh.html#a4b04262b81aa7d31eb5d2f607e2a35de',1,'my_types.hh']]],
  ['removeparticle_156',['removeParticle',['../class_system.html#a8b8b7f24d3953b4cb54160095a3abec7',1,'System']]],
  ['resize_157',['resize',['../struct_matrix.html#a9f9183cb0a7638ada0e66275ee79940a',1,'Matrix']]],
  ['results_5fq2_158',['Results_q2',['../class_results__q2.html',1,'']]],
  ['results_5fq3_159',['Results_q3',['../class_results__q3.html',1,'']]],
  ['results_5fq4_160',['Results_q4',['../class_results__q4.html',1,'']]],
  ['rows_161',['rows',['../struct_matrix.html#a7e8c7a0dfb87597abeb675d36650b792',1,'Matrix']]]
];

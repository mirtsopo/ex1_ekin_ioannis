#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
  //get number of particles and size of the matrix representing the system
  int N = system.getNbParticles();
  int size= std::sqrt(N);
  //create complex matrices for temperature, heat source and frequencies
  Matrix<complex> theta(size);
  Matrix<complex> h(size);
  Matrix<std::complex<double>> freq = FFT::computeFrequencies(size);

  //put the temperature of each material point to its corresponding position in the matrix
  for (auto&& entry : index(theta)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    auto& mat_point = static_cast<MaterialPoint&>(system.getParticle(j+i*size));
    val = mat_point.getTemperature();
  }

  //put the heat source of each material point to its corresponding position in the matrix
  for (auto&& entry : index(h)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    auto& mat_point = static_cast<MaterialPoint&>(system.getParticle(j+i*size));
    val = mat_point.getHeatRate();
  }

  // take fft transform of the temperature and heat source matrix with correct dimensions
  Matrix<complex> theta_hat = FFT::transform(theta);
  theta_hat /= (1.0/size);
  Matrix<complex> h_hat = FFT::transform(h);
  //create complex matrix for time derivate of temperature in fourier domain
  Matrix<complex> del_theta_hat(size);

  //calculate time derivate of temperature in fourier domain at each position
  for (auto&& entry : index(del_theta_hat)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    h_hat(i,j) /= 1.0/size;
    val = h_hat(i,j) - theta_hat(i,j)*freq(i,j);
  }

  //calculate the time derivatine in real domain via inverse fourier transform
  Matrix<complex> del_theta = FFT::itransform(del_theta_hat);

  //increment temperature
  for (auto&& entry : index(theta)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val += this->Deltat * del_theta(i,j);
  }

  //if zero-boundary must be enforced, drive the temparature to zero at the limits
  if (boundaries){
    for (auto&& entry : index(theta)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      if (i == 0 || i == size-1 || j == 0 || j == size-1){
        auto& val = std::get<2>(entry);
        val = 0;
      }
    }
  }

  //update the material points
  int i=0;
  for (auto &par : system) {
    auto& mat_point = static_cast<MaterialPoint&>(par);
    mat_point.getTemperature() = std::real(theta(i/size,i%size));
    mat_point.getHeatRate() = std::real(h(i/size,i%size));
    i+=1;
  }
}
//method to determine timestep
void ComputeTemperature::setDeltat(double timestep){this->Deltat = timestep;}
//method to determine boundary type
void ComputeTemperature::setBoundaries(bool boundaries){this->boundaries = boundaries;}
/* -------------------------------------------------------------------------- */

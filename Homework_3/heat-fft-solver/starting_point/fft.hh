#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);
  static Matrix<std::complex<double>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  //create fft plan and matrix to output the results
  fftw_plan p;
  unsigned int N = m_in.size();
  Matrix<complex> out(N);

  //set the fft plan
  p = fftw_plan_dft_2d(N, N, reinterpret_cast<fftw_complex*>(m_in.data()), reinterpret_cast<fftw_complex*>(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
  //run the fft
  fftw_execute(p);
  //delete the plan
  fftw_destroy_plan(p);
  //return the matrix
  return out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  //create fft plan and matrix to output the results
  fftw_complex *in;
  fftw_plan p;
  unsigned int N = m_in.size();
  Matrix<complex> out(N);

  //set the fft plan
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
  p = fftw_plan_dft_2d(N, N, reinterpret_cast<fftw_complex*>(m_in.data()), reinterpret_cast<fftw_complex*>(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
  //run the fft
  fftw_execute(p);
  //delete the plan
  fftw_destroy_plan(p);
  fftw_free(in);
  //return the matrix
  out /= N*N;
  return out;
}

/* ------------------------------------------------------ */


inline Matrix<std::complex<double>> FFT::computeFrequencies(int size) {
    std::vector<double> f;
    std::vector<double> f_back;
    if (size>1){
        // size is even
        if (size % 2 == 0){
            for(int i = 0; i< size/2; i++){
                f.push_back((size - double(size-i))/size);
            }
            f.push_back(-size/2.0/size);

            f_back = std::vector<double>(f.begin()+1, f.end() - 1);  // make a sliced version of the front part array
        }

        // size is odd
        else{
            for(int i = 0; i< size/2 + 1; i++){
                f.push_back((size - double(size-i))/size);
            }
            f.push_back(-(size-1.0)/2.0/size);

            f_back = std::vector<double>(f.begin()+1, f.end() - 2);                        // make a sliced version of the front part array
        }
        std::transform(f_back.begin(), f_back.end(), f_back.begin(), [](double& c){ return c*-1;});
        std::reverse(f_back.begin(), f_back.end());                                                 // reverse the back part array
        f.insert( f.end(), f_back.begin(), f_back.end() );                                          // concatenate the front and back arrays
    }
    else{
        f.push_back(0.0);
    }
    //calculate the q_x^2 + q_y^2 values
    Matrix<std::complex<double>> freq_m(size);
    for (auto&& entry : index(freq_m)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        std::complex<double> temp (f[i]*M_PI, f[j]*M_PI);
        val = pow(std::abs(temp),2.0);
    }

    return freq_m;
}

#endif  // FFT_HH

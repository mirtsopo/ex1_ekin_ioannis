# -*- coding: utf-8 -*-
"""
Spyder Editor

This script creates input files for the fft code with certain initial
conditions for heat source and temperature fields. 
Mandatory arguments;
    N: number of points in each direction. NxN arrays will be formed to
    represent the fields
    R: geometric parameter necessary for Exercise 4.5
"""
import numpy as np
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("N", help="number of points in each direction", type=int)
parser.add_argument("R", help="geometric parameter [0,1]", type=float)
if len(sys.argv) != 3:
    print("Wrong number of enough arguments entered")
    sys.exit()

args = parser.parse_args()

if args.R >= 1 or args.R < 0:
    print("R should be in [0,1)")
    sys.exit()

N = args.N
R = args.R

#define a function that generates 2D field, represented in 2D numpy array
def create_field(function, *args):

    field = np.zeros((N,N),dtype=complex)
    x_pos = np.zeros((N,N),dtype=complex)
    y_pos = np.zeros((N,N),dtype=complex)

    for i in range(N):
        for j in range(N):
            x = np.round(-1 + 2*j/(N-1),8)
            y = np.round(1 - 2*i/(N-1),8)
            y_pos[i,j] = y
            x_pos[i,j] = x
            field[i,j] = function(x,y, args)

    return x_pos, y_pos, field

#function to write data to a file in requires fashion
def write_temp_data(x_pos, y_pos, temp_field, heat_field, filename):
    N = len(x_pos)
    f = open("cmake-build-debug/" + filename, "w")
    standard_text = " 0 0 0 0 0 0 0 1 "
    for i in range(N):
        for j in range(N):
            f.write(str(np.real(x_pos[i,j])) + " " +str(np.real(y_pos[i,j])) + standard_text +  str(np.real(temp_field[i,j])) + " " + str(np.real(heat_field[i,j]))+"\n")
    f.close()

#define functions for heat source and temperature field for Exercise 4.3
def heat_source_1(x,y, *args):
    return  np.round(np.sin(np.pi*x)*np.pi**2,8)/args[0][0]**2

def temperature_field_1(x,y,*args):
    return np.round(np.sin(np.pi*x),8)

def heat_source_2(x,y,*args):
    if x == 0.5:
        return 1/args[0][0]
    elif x == -0.5:
        return -1/args[0][0]
    else:
        return 0

def temperature_field_2(x,y,*args):
    if x < -0.5:
        return -x-1
    elif (x >= -0.5) and (x <= 0.5):
        return x
    else:
        return -x+1

def heat_source_3(x,y, *args):
    if x**2 + y**2 < args[0][1]:
        return 1/args[0][0]**2
    else:
        return 0

def temperature_field_3(x,y, *args):
    return 0

#define number of points in each direction, create 2D field variables
# and write to a .csv file

x_pos, y_pos, heat_field = create_field(heat_source_1, N)
x_pos, y_pos, temp_field = create_field(temperature_field_3, N)

write_temp_data(x_pos, y_pos, temp_field, heat_field, "temp_data_q3_init.csv")

x_pos, y_pos, temp_field = create_field(temperature_field_1, N, R)
write_temp_data(x_pos, y_pos, temp_field, heat_field, "temp_data_q3_final.csv")


#define number of points in each direction, create 2D field variables
# and write to a .csv file

x_pos, y_pos, heat_field = create_field(heat_source_2, N)
x_pos, y_pos, temp_field = create_field(temperature_field_3, N)

write_temp_data(x_pos, y_pos, temp_field, heat_field, "temp_data_q4_init.csv")

x_pos, y_pos, temp_field = create_field(temperature_field_2, N, R)
write_temp_data(x_pos, y_pos, temp_field, heat_field, "temp_data_q4_final.csv")


#define number of points in each direction and geometric parameter R,
# create 2D field variables and write to a .csv file
x_pos, y_pos, heat_field = create_field(heat_source_3, N, R)
x_pos, y_pos, temp_field = create_field(temperature_field_3, N)

write_temp_data(x_pos, y_pos, temp_field, heat_field, "temp_data_q5_init.csv")

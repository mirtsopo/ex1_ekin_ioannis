#include "fft.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

// Fixture class for Exercise 4, Question 2
class Results_q2 : public ::testing::Test {
protected:
  void SetUp() override {

    // the number of steps to perform
    Real nsteps=2000;
    // freq to dump
    int freq=10;
    // init file
    std::string filename = "temp_data_q2_init.csv";
    // particle type
    std::string type = "material_point";
    // timestep
    Real timestep=0.015;

    //create a simulation and evolve certain number of steps
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, timestep);

    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    //store temperature values in a vector
    N = evol.getSystem().getNbParticles();
    auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(0));
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(i));
      sim_results.push_back(mat_point.getTemperature());
    }

    //get temperature data of analytic solution
    ParticlesFactoryInterface& factory_final = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol_final = factory_final.createSimulation("temp_data_q2_final.csv", 0);

    //store temperature values in a vector
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol_final.getSystem().getParticle(i));
      ideal_results.push_back(mat_point.getTemperature());
    }

  }
  std::vector<double> ideal_results;
  std::vector<double> sim_results;
  int N;
};
/*****************************************************************/
// Test for Exercise 4, Question 2
TEST_F(Results_q2, Question2) {

  //check if values are close
  for(int i = 0; i<N; i++){
    float rel_err = (ideal_results[i] - sim_results[i]);
    ASSERT_NEAR(rel_err , 0, 1e-5);
  }
}
// Fixture class for Exercise 4, Question 3
class Results_q3 : public ::testing::Test {
protected:
  void SetUp() override {

    // the number of steps to perform
    Real nsteps=2000;
    // freq to dump
    int freq=10;
    // init file
    std::string filename = "temp_data_q3_init.csv";
    // particle type
    std::string type = "material_point";
    // timestep
    Real timestep=0.015;

    //create a simulation and evolve certain number of steps
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, timestep);

    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    //store temperature values in a vector
    N = evol.getSystem().getNbParticles();
    auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(0));
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(i));
      sim_results.push_back(mat_point.getTemperature());
    }

    //get temperature data of analytic solution
    ParticlesFactoryInterface& factory_final = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol_final = factory_final.createSimulation("temp_data_q3_final.csv", 0);

    //store temperature values in a vector
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol_final.getSystem().getParticle(i));
      ideal_results.push_back(mat_point.getTemperature());
    }

  }
  std::vector<double> ideal_results;
  std::vector<double> sim_results;
  int N;
};
/*****************************************************************/
// Test for Exercise 4, Question 3
TEST_F(Results_q3, Question3) {

  //check if values are close
  for(int i = 0; i<N; i++){
    float rel_err = (ideal_results[i] - sim_results[i]);
    ASSERT_NEAR(rel_err , 0, 0.2);
  }
}

// Fixture class for Exercise 4, Question 4
class Results_q4 : public ::testing::Test {
protected:
  void SetUp() override {

    // the number of steps to perform
    Real nsteps=2000;
    // freq to dump
    int freq=10;
    // init file
    std::string filename = "temp_data_q4_init.csv";
    // particle type
    std::string type = "material_point";
    // timestep
    Real timestep=0.015;

    //create a simulation and evolve certain number of steps
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, timestep);

    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    //store temperature values in a vector
    N = evol.getSystem().getNbParticles();
    auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(0));
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(i));
      sim_results.push_back(mat_point.getTemperature());
    }

    //get temperature data of analytic solution
    ParticlesFactoryInterface& factory_final = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol_final = factory_final.createSimulation("temp_data_q4_final.csv", 0);

    //store temperature values in a vector
    for(int i = 0; i<N; i++){
      auto& mat_point = static_cast<MaterialPoint&>(evol_final.getSystem().getParticle(i));
      ideal_results.push_back(mat_point.getTemperature());
    }

  }
  std::vector<double> ideal_results;
  std::vector<double> sim_results;
  int N;
};
/*****************************************************************/
// Test for Exercise 4, Question 4
TEST_F(Results_q4, Question4) {

  //check if values are close
  for(int i = 0; i<N; i++){
    float rel_err = (ideal_results[i] - sim_results[i]);
    ASSERT_NEAR(rel_err , 0, 0.2);
  }
}


TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;


    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {

  UInt N = 10;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;

  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = 2*cos(k * i)+cos(k * j)*sin(2*k*i);
  }

  Matrix<complex> res = FFT::transform(m);
  //res /= N;
  Matrix<complex> inv_res = FFT::itransform(res);
  //inv_res /= N;
  

    for (auto&& entry : index(inv_res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    //std::cout << std::abs(val) << "\t" << std::abs(m(i,j)) << std::endl;

    ASSERT_NEAR(std::abs(val), std::abs(m(i,j)), 1e-10);

    /*if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), val2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
      */

  }
}
/*****************************************************************/

TEST(FFT, computeFrequencies) {
    int N = 21;

    Matrix<std::complex<double>> freq_m = FFT::computeFrequencies(N);

    for (auto&& entry : index(freq_m)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      auto& val = std::get<2>(entry);

      if (std::abs(val) < 1e-10)
        std::cout << i << "," << j << " = " << val << std::endl;

      ASSERT_NEAR(std::abs(freq_m(j, i)), std::abs(freq_m(i, j)), 1e-10);
      ASSERT_TRUE(std::abs(freq_m(i, j)) >= 0);
    }
}

/*
TEST(FFT, Question4) {

  // the number of steps to perform
  Real nsteps=2000;
  // freq to dump
  int freq=10;
  // init file
  std::string filename = "temp_data_q3_init.csv";
  // particle type
  std::string type = "material_point";
  // timestep
  Real timestep=0.015;

  MaterialPointsFactory::getInstance();
  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol = factory.createSimulation(filename, timestep);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);
  evol.evolve();

  int N = evol.getSystem().getNbParticles();
  std::vector<double> sim_results;
  auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(0));
  for(int i = 0; i<N; i++){
    mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(i));
    sim_results.push_back(mat_point.getTemperature());
  }

  ParticlesFactoryInterface& factory_final = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol_final = factory_final.createSimulation("temp_data_q3_final.csv", 0);

  std::vector<double> ideal_results;
  for(int i = 0; i<N; i++){
    mat_point = static_cast<MaterialPoint&>(evol_final.getSystem().getParticle(i));
    ideal_results.push_back(mat_point.getTemperature());
  }
  for(int i = 0; i<N; i++){

      float rel_err = (ideal_results[i] - sim_results[i]);
      ASSERT_NEAR(rel_err , 0, 0.15);


  }

}

TEST(FFT, Question5) {

  // the number of steps to perform
  Real nsteps=2000;
  // freq to dump
  int freq=10;
  // init file
  std::string filename = "temp_data_q4_init.csv";
  // particle type
  std::string type = "material_point";
  // timestep
  Real timestep=0.015;

  MaterialPointsFactory::getInstance();
  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol = factory.createSimulation(filename, timestep);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);
  evol.evolve();

  int N = evol.getSystem().getNbParticles();
  std::vector<double> sim_results;
  auto& mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(0));
  for(int i = 0; i<N; i++){
    mat_point = static_cast<MaterialPoint&>(evol.getSystem().getParticle(i));
    sim_results.push_back(mat_point.getTemperature());
  }

  ParticlesFactoryInterface& factory_final = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol_final = factory_final.createSimulation("temp_data_q4_final.csv", 0);

  std::vector<double> ideal_results;
  for(int i = 0; i<N; i++){
    mat_point = static_cast<MaterialPoint&>(evol_final.getSystem().getParticle(i));
    ideal_results.push_back(mat_point.getTemperature());
  }
  for(int i = 0; i<N; i++){

    float rel_err = (ideal_results[i] - sim_results[i]);
    ASSERT_NEAR(rel_err , 0, 0.2);


  }

}
*/
// Fixture class


/*****************************************************************/

#include <iostream>
#include <string>
#include <memory>
#include "Series.hh"
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "PrintSeries.hh"
#include "DumperSeries.hh"
#include "WriteSeries.hh"
#include <fstream>
using namespace std;

int main(int argc, char ** argv) {

    //CHECK IF USER INPUT IS CORRECT
    if (argc != 6){
        std::cerr << "Not enough arguments entered!" << std::endl;
        return EXIT_FAILURE;
    }

    // ASSIGN USER INPUT TO VARIABLES
    std::string series_type = argv[1];
    std::string output_type = argv[2];
    std::string separator_type = argv[3];
    int maxiter = std::stoi(argv[4]);
    int freq = std::stoi(argv[5]);

    // CREATE SERIES OBJECT WITH REQUIRED SERIES TYPE
    std::unique_ptr<Series> Series;
    if (series_type == "Arithmetic"){
        Series = std::make_unique<ComputeArithmetic>();
    }
    else if (series_type == "Pi"){
        Series = std::make_unique<ComputePi>();
    }
    else {
        std::cout << "Invalid Series Type Entered" << std::endl;
        return EXIT_FAILURE;
    }

    // CREATE OSTREAM OBJECT AND USE IN RELEVANT OUTPUT TYPE
    ofstream os;
    if (output_type == "Write"){
        WriteSeries write_series(maxiter, *Series);
        write_series.setPrecision(10);
        write_series.setSeperator(separator_type);
        write_series.dump(os);
    }
    else if (output_type == "Print"){
        PrintSeries print_series(maxiter,freq,*Series);
        print_series.setPrecision(10);
        print_series.dump(os);
    }
    else {
        std::cout << "Invalid Output Type Entered" << std::endl;
        return EXIT_FAILURE;
    }

    return 0;
}
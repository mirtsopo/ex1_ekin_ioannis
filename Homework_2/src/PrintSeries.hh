#ifndef PrintSeries_HH
#define PrintSeries_HH
#include "DumperSeries.hh"

class PrintSeries: public DumperSeries {
public:
    // constructor
    PrintSeries(int maxiter, int freq, Series &series);
    // members
    int maxiter;
    int freq;
    // methods
    void dump(std::ostream & os) override;
};

#endif
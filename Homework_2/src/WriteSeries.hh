#ifndef WriteSeries_HH
#define WriteSeries_HH
#include "DumperSeries.hh"
#include <string>

class WriteSeries: public DumperSeries {
public:
    // constructor
    WriteSeries(int maxiter, Series &series);
    // members
    int maxiter;
    std::string delimiter;
    std::string file_name;
    enum fileType {csv , psv, txt};
    // methods
    void dump(std::ostream & os) override;
    void setSeperator(std::string delimiter);

};

#endif

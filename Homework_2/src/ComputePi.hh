#ifndef ComputePi_HH
#define ComputePi_HH
#include "Series.hh"

class ComputePi: public Series {
public:
    // constructor
    ComputePi();
    //methods
    double compute(unsigned int N) override;
    void addTerm();
    float computeTerm(float k);
    double getAnalyticPrediction() override;
};

#endif
#ifndef Series_HH
#define Series_HH

class Series{
public:
    // constructor
    Series();
    // members
    unsigned int current_index;
    double current_value;
    //abstract methods
    virtual double compute(unsigned int N)=0;
    virtual double getAnalyticPrediction();
};

#endif
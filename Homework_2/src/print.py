# -*- coding: utf-8 -*-
import os
import argparse
import matplotlib.pyplot as plt

def printsavedfile(file_loc, separator):
    """
    function to read data from .cvs .psv or .txt file and plot
    
    Parameters
    ----------
    file_loc : file path
        path of the data file that would be read and plotted
    separator : string
        string used when the data file was written, see print.py -h to see the options

    Returns
    -------
    None.

    """
    # go to the directed directory and create a string for the full path of the file to be read
    os.chdir(file_loc) 
    extensions = {",": ".csv", "|": ".psv", " ": ".txt", "\\t": ".txt"}
    full_path = file_loc + "/series_data" + extensions[separator]
    
    # read the data line by line and store in empty array if the file exists
    try:
        with open(full_path, "r") as f:
            lines = f.readlines()
            linesNo = len(lines)
    
            it = []
            val = []
            for i in range(linesNo-1):
                it.append(int(lines[i].split(separator)[0]))
                val.append(float(lines[i].split(separator)[1]))   
    except:
        print ("No file of such file format exists in the given directory")
         
    # plot the data and save as .png file
    fig = plt.figure()
    axe = fig.add_subplot(1, 1, 1) 
    axe.scatter(it, val, marker='o')
    axe.set_xlabel('Iteration')
    axe.set_ylabel('Series value')
    plt.savefig("series_plot.png")
    
        
parser = argparse.ArgumentParser(description='Input a file path to print and its extension')

# declare positional arguments
parser.add_argument("file_location", 
                    help="define the location where to look for the file", 
                    type = str
                    )
parser.add_argument("separator", 
                    help="define the separator", 
                    choices = [",","|"," ","\\t"],
                    type = str
                    )

args = parser.parse_args()

#get the file location and its extension
file_loc = args.file_location
sep = args.separator

#call the function to print the file with the desired extension
printsavedfile(file_loc, sep)
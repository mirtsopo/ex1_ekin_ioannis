#ifndef ComputeArithmetic_HH
#define ComputeArithmetic_HH
#include "Series.hh"

class ComputeArithmetic: public Series {
public:
    // constructor
    ComputeArithmetic();
    // methods
    double compute(unsigned int N) override;
    void addTerm();
    float computeTerm(int k);
    double getAnalyticPrediction() override;
};

#endif
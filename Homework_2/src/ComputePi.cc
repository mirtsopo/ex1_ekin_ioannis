#include "ComputePi.hh"
#include <math.h>

    ComputePi::ComputePi() {};

    double ComputePi::compute(unsigned int N){
        //check the index wrt given N
        if (this->current_index <= N){
            N -= this->current_index;
        }
        else{
            this->current_value = 0;
            this->current_index = 0;
        };
        // add term to members using addTerm method
        for (int k = 0; k < N; k++){
            this->addTerm();
        }

        return sqrt(6*this->current_value);

    };
    // method to update current term and serie value
    void ComputePi::addTerm(){
        this->current_index += 1;
        this->current_value += this->computeTerm(this->current_index);
    };
    // method to pull the terms of the series
    float ComputePi::computeTerm(float k){
        return 1/(k*k);
    };
    // method to obtain the analytical prediciton of the series
    double ComputePi::getAnalyticPrediction() {
        return M_PI;
    }



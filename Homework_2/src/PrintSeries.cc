#include "PrintSeries.hh"
#include <cstdlib>
#include <string>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>
#include <typeinfo>

PrintSeries::PrintSeries(int maxiter, int freq, Series &series):DumperSeries(series){
    this->maxiter =maxiter;
    this->freq = freq;
};

void PrintSeries::dump(std::ostream & os = std::cout) {
    // create empty string which will store the value to be written
    std::string out;
    // for each iteration, calculate necessary values, store them in the string
    for (int i = 1; i < int(this->maxiter/this->freq); i++) {

        //calculate values for the iteration and the previous state for comparison
        double res = this->series.compute(i * this->freq - 1);
        double res2 = this->series.compute(i * this->freq);

        //create strings to store the values of the series and the difference with previous step
        std::stringstream r1;
        std::stringstream r2;
        r1 << std::scientific << std::setprecision(this->precision) << res;
        r2 << std::scientific << std::setprecision(this->precision) << res2 - res;

        //store the calculated values on the empty string created
        out += std::to_string(i * this->freq) + " ";
        out += r1.str() + " ";
        out += r2.str() + " ";

        // get the analytical prediction if applicable
        try{
            std::stringstream diff;
            diff << std::setprecision(this->precision) << res2 - this->series.getAnalyticPrediction();
            out += diff.str();
            out += "\n";

        }
        catch (...){
            // DOES NOTHING
        }
    }
    // create ofstream object, open it, write the store values in it and close
    std::cout << out << std::endl;
    //os << out << std::endl;

};

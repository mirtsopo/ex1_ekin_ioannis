cmake_minimum_required(VERSION 2.6)
project(homework-2)
set (CMAKE_CXX_STANDARD 17)

add_executable(main main.cc Series.cc ComputeArithmetic.cc ComputePi.cc DumperSeries.cc PrintSeries.cc WriteSeries.cc)

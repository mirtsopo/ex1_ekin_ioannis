**Exercise 2 - Scientific Programming for Engineers**

------------------------------------------------------
Description
------------------------------------------------------
This repository contains families of classes written in C++. Their functionalities are framed around plotting and dumping arithmetic series, following the user's preferences.

------------------------------------------------------
Requirements
------------------------------------------------------
The code is tested with the following versions
- Python 3.8.3
- Argparse 1.1
- C++17
- Clion 2020.2.3

------------------------------------------------------
Instructions
------------------------------------------------------
To obtain the repository run the code below on terminal:

`git clone git@gitlab.epfl.ch:mirtsopo/ex1_ekin_ioannis.git` // clone the repo

The main directory of the repository should include the following files and folders:

--**src**   (folder) Contains the following .hh and .cc files:

- **ComputeArithmetic.hh**:    A child class of "Series". The file contains methods that must be implemented ("compute" and "getAnalyticPrediction"), as well as class-specific methods. No members are defined, as they are inherited from "Series" class
- **ComputeArithmetic.cc**:    The file contains the implementations of the methods defined in the respective .hh file. The methods compute series terms using class members and retrieve analytical solution.
- **ComputePi.hh**:     A child class of "Series". The file contains methods that must be implemented ("compute" and "getAnalyticPrediction"), as well as class-specific methods. No members are defined, as they are inherited from "Series" class
- **ComputePi.cc**:    The file contains the implementations of the methods defined in the respective .hh file. The methods compute series terms using class members and retrieve analytical solution.
- **DumperSeries.hh**:    The file contains members and the abstract methods that need to be implemented
- **DumperSeries.cc**:    The file contains abstract methods and methods that would be inherited by its child classes.
- **PrintSeries.hh**:     A child class of “DumperSeries” class. The file contains the class constructor, its members and the function (“dump”) that must be implemented. The function is overridden because its implementation differs from the implementation in its motherclass (“DumperSeries”).
- **PrintSeries.cc**:    The file contains the implementation of the constructor, where its members are assigned to values and the implementation of the abstract class “dump”, as imposed by the mother class.The class takes care of the plot-on-the-console operations.
- **Series.hh**:        The file contains the members and the abstract methods that need to be implemented. The class has no constructor.
- **Series.cc**:        The file contains the abstract methods. There is no real implementation for them. The classes that use “Series” class as an input will have to override these methods. Overriden methods calculate the series and retrieve analytical predictions (if available).
- **WriteSeries.hh**:    A child class of “DumperSeries” class. The file contains the class constructor, its members, an enum list of file types and the functions (“dump” and “setSeparator”) that must be implemented. The function “dump” is overridden because its implementation differs from the implementation in its motherclass (“DumperSeries”).
- **WriteSeries.cc**:    The file contains the implementation of the constructor, where its members are assigned to values and the implementation of the abstract class “dump” and “setSeparator”, as imposed by the mother class. The class takes care of the write-to-file operations.
- **main.cc**:    The file which calls the respective functions based on the user’s preferences (arithmetic/pi, print/write, file type, maximum iteration and frequency)
- **print.py**:     The file reads the written files (.csv, .psv or .txt) in the given path and plots the numerical results onto a .png file.

--**README.md**:    File that contains the necessary documentation.

--**.gitignore**:   File that includes the list of files to be excluded from the version control process.

Compile main using Clion by creating a "build" directory next to src. In Clion we have to set the "../Homework_2/build" folder as the destination for the compiled executable. Any data files written will also be written to "build" directory.

--**build**     (folder) Empty folder where he compiled "main" will be saved if the build directory is chosen as indicated above. All written files and plots will be saved in this directory too.

------------------------------------------------------
File overview
------------------------------------------------------
- **main.cc**: 5 arguments must be given in the order explained in the Instructions. Arguments are cast into relevant variables and necessary Series and DumperSeries objects are created and necessary methods are called depending on the input. If the arguments don't match with the described options or incorrect number of arguments given, the code will exit without success. Inputs Pi and Arithmetic run the code for the pi series and arithmetic series, respectively. Inputs Write and Print, writes the data on a file or prints on the colsole. Inputs [, | " "] write the data using those separators and writes .csv, .psv or .txt files if Write argument is given. They print the data on the console using those separators if Print argument is given. Arguments 4 and 5 determine the maximum iterations done for the series calculation and the frequency of output if the data would be written to a file. (see Example Usage section for more details).

- **python.py**: 2 arguments must be given when executed from the terminal. The user provides the path of the written files and the file extension of the file to read. The numerical results of the file are plotted onto a .png file. If comma (,) is given, the content of the .csv file will be plotted. Respectively, if the pipe separator (|) is given, the content of the .psv file will be plotted. If space or TAB ( ) or (\t) is given, the content of the .txt file will be plotted. By default the content of the .txt file is plotted. If the given separator does not match to the existing written files, an informative error message is returned respectively (see Example Usage section for more details).
 
The file makes use of `argparse`. If additional documentation is needed, the user is invited to type:

`python3 print.py -h`

------------------------------------------------------
Key concepts
------------------------------------------------------
The concept of abstract methods perfectly matches the idea of collaborative scripting projects. All developers are aware of the types of the input variables they need to use, as well as the type of the output variables the methods will compute. Additionally, the implementation of these methods can not be forgotten/skipped. Inheritance is also usefull in collaborative work features of classes that have been implemented are shared with children. 

The complexity of the series calculation without storing the index and the cumulative values goes by N^2 where N is the number of terms. For each term, the summation is started over and this increases the complexity. 

The updated applications stores the values, so recalculations are avoided. The complexity of the scheme goes with N. 

If the summation is to be started from the biggest number, than it wouldn't be possible to store the series value and add the next iteration because its precision would be lost. Then, the previous implementation must be used. It is possible to group the numbers according to their contribution to the overall precision. (2 and 3 contributes to tenths digit, 4-10 contributes to hundreds digit etc.) Then, series values of groups can be stored and complexity N^2 scheme would be used within groups and complexity N scheme would be used when summing the groups. This results in a complexity between N and N^2. 

------------------------------------------------------
Example Usage
------------------------------------------------------
Compile project in Clion. The directory at which the compilation should happen can be controlled from "`File>Settings>Build, Execution, Deployment>cmake>Profiles`". For this project, "`../Homework_2/build`" directory was chosen.

Go to the directory `../Homework_2/build`
In the terminal execute `./main Pi Write , 100 5`             --> calculates Pi series for 100 iterations with frequency of 5 and writes to a .csv file

Go to the directory `../Homework_2/src`
In the terminal execute `python3 print.py <file_path> ','`    --> reads .csv file from the given file path, plots the series vs. iteration graph and saves

------------------------------------------------------
Authors
------------------------------------------------------
- R. Ekin Kubilay
- Ioannis Mirtsopoulos

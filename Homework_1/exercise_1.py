import numpy as np
import optimizer

# The following function could be moved to optimizer.py
# Keeping it here, makes the optimizer.py a "library" file 
# where no function is stored, whereas many functions can call it
def calc_func(x):
    """
    A function to calculate the function: S(x) = x.T*A*x - x.T*B
    ========== Parameters ==========
    x : vector
    The vector to calculate the function for.
    ========== Return ==========
    int
    The value of the function S(x) at x
    """
    # build A matrix
    a = [[4, 0], [1, 3]]
    A = np.array(a)

    # build B vector
    b = [[0, 1]]
    B = np.array(b).T
    
    return np.dot(np.dot(x.T,A),x) - np.dot(x.T,B)

def main():
    # set initial guess
    guess = [-5,5]
    initial_guess = np.array(guess)
    
    # initialize the optimizer with the initial guess
    optimizer.init(initial_guess)
    # call the optimizer
    res = optimizer.minimize_function(calc_func, initial_guess, 0)    
    # plot progress
    optimizer.x_progress = np.array(optimizer.x_progress)    
    optimizer.plot_process(calc_func, optimizer.x_progress, 'EX1_BFGS')
    # prints a summary of the optimization (suceess/fail, no of iterations etc.)
    print(res) 
    
    # initialize the optimizer with the initial guess
    optimizer.init(initial_guess)
    # call the optimizer
    res = optimizer.minimize_function(calc_func, initial_guess, 1)    
    # plot progress
    optimizer.x_progress = np.array(optimizer.x_progress)    
    optimizer.plot_process(calc_func, optimizer.x_progress, 'EX1_CG')
    # prints a summary of the optimization (suceess/fail, no of iterations etc.)
    print(res) 

if __name__ == '__main__':
    main()
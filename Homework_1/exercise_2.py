import argparse
import main

def read_csvfile(file):
    """
    Function that gets matrices or vectors from .csv files
    and turns them into lists of lists or lists respectively
    ========== Parameters ==========
    file : str
    Describes the name of the .csv file to precess
    ========== Returns ==========
    x : list or list of lists
    Describes the matrix or the vector respectively
    """
    with open(file, "r") as f:
        lines = f.readlines()
        rowsNo = len(lines)
        x = []
        for i in range(rowsNo):
            cell = lines[i].split(',')
            columnsNo = len(cell)
            if  columnsNo == 1: x.append(float(cell[0]))
            else: x.append([float(cell[i]) for i in range(columnsNo)])
    return x

parser = argparse.ArgumentParser(description='Input a quadratic function, decide the optimizer to find its minimum value')

# declare positional arguments
parser.add_argument("A", 
                    help="feed this equation S(x) = x.T*A*x - x.T*B with a .csv file which includes A", 
                    type = str
                    )
parser.add_argument("B", 
                    help="feed this equation S(x) = x.T*A*x - x.T*B with a .csv file which includes B", 
                    type = str
                    )

# declare optional arguments
parser.add_argument("-opt", "--optimizer", 
                    help="choose the optimizer; 0: scipy.BFGS, 1: scipy.CG, 2: conjugate gradient without libraries", 
                    type = int,
                    choices = [0,1,2],
                    default = 0
                    )

parser.add_argument("-plt", "--plotting", 
                    help="choose plot options; 0: without plots, 1: with plots", 
                    type = int,
                    choices = [0,1],
                    default = 0
                    )
args = parser.parse_args()

#create A and B matrices
matrix = read_csvfile(args.A)
vector = read_csvfile(args.B)

#call the function from main to do the optimization wtih chosen method and plotting option
main.basic_func(matrix, vector, args.optimizer, args.plotting)
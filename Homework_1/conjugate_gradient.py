import numpy as np

def conjgrad(A, b, x):
    """
    A function to solve A*x = b with the conjugate gradient method.
    Pseudocode taken from https://en.wikipedia.org/wiki/Conjugate_gradient_method (Chap. "The resulting algorithm")
    ========== Parameters ==========
    A : matrix 
	A real symmetric positive definite matrix.
    b : vector
	The right hand side vector of the system.
    x : vector
	The starting guess for the solution.
    ========== Return ==========
    x_evolution numpy array
    The array of x values obtained from each step of the iteration
    """  
    #checking if matrix A is symmetric, prints a warning statement if not and
    #quits without applying the method
    A = np.array(A)
    b = np.array(b)
    if (A!=A.transpose()).any():        
        print("This method requires A to be symmetric")
        return 0
        
    #checking if matrix A is positive definite, prints a warning statement if 
    #not and quits without applying the method
    if (np.linalg.eig(A)[0] < 0).any():
        print("This method requires A to be positive definite")
        return 0

    #store x for plotting purposes
    x_evolution = []
    x_evolution.append(x)    
    
    r = b - np.einsum("ik, k->i", A, x) # equivalent to r = A*x

    p = r
    
    rsold = np.einsum("i,i->", r, r) # equivalen to rsold = r.T*r

    # break condition; # if the r is sufficiently small exit the loop 
    # create a counter to stop iteration if process diverges
    i = 0
    while (np.linalg.norm(r) > 1e-8):
        Ap = np.einsum("ik, k->i", A, p) # equivalent to Ap = A*p

        alpha = rsold / np.einsum("i,i->", p, Ap) # equivalent to alpha = rsold/(p.T*Ap)

        x = x + np.einsum(",k->k", alpha, p) # equivalent to x = x + alpha*p

        r = r - np.einsum(",k->k", alpha, Ap) # equivalent to r = r - alpha*Ap
        
        rsnew = np.einsum("i,i->", r, r) # equivalent to rsnew = r.T*r
       
        p = r + (rsnew/rsold)*p
        
        rsold = rsnew
        #store the value obtain from this iteration
        x_evolution.append(x)
        #increment i and check if the number of iterations is large
        i+=1
        if i > 1000:
            print('Did not converge')
            break
        
    #return the progress of the iteration as numpy array
    return np.array(x_evolution)
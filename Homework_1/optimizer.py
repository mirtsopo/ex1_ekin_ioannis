import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def init(guess):
    """
    The function that initializes global paramaters to be used across modules
    ========== Parameters ==========
    guess : np.array
    The initial guess for the iterative minimization process.
    ========== Returns ==========
    None
    """
    #define global variable x_progress, initialize by an empty list and
    #append the initial guess
    global x_progress
    x_progress = []
    x_progress.append(guess)

def minimize_function(func, guess, my_method):
    """
    The function that calls scipy to minimize the quadratic equation while doing 
    callback to a storing function
    ========== Parameters ==========
    func : function
        function representing the expression to be minimized
    guess : np.array 
    The initial guess for the iterative minimization process
    ========== Returns ==========
    OptimizeResult Object
    The optimized result object as described in scipy documentation.
    """
    #return the object created from scipy minimize
    if my_method==0:
        return minimize(func, guess, method='BFGS', tol=1e-6, callback=store)
    elif my_method==1:
        return minimize(func, guess, method='CG', tol=1e-6, callback=store)


def store(x):
    """
    The function that stores result of each iteration of the optimization 
    in global variable x_progress   
    ========== Parameters ==========
    x : list of lists
    Consists of position of minimums found from each optimization step.
    ========== Returns ==========
    None
    """
    #appends the value of x to the global parameter x_progress
    x_progress.append(x)

def plot_process(func, x_evolution, method_name):
    """
    A function that takes care of plotting graphs
    ========== Parameters ==========
    func : programming function
    A function which represents the quadratic equation
    x_evolution : list
    A list representing the x-vector solution for all iterations
    ========== Returns ==========
    None
    """

    # store the solution of each iteration step in func_value
    func_value = np.zeros(len(x_evolution))
    for i,val in enumerate(x_evolution):
        func_value[i] = func(val)
    
    # determine the plot range for 2D plot (extend 20% for visualization)
    x_range = np.max(np.abs(x_evolution[:,0])*1.2)
    y_range = np.max(np.abs(x_evolution[:,1])*1.2)
    
    # create the plot domain for 3D plot
    x_axis = np.linspace(-x_range,x_range,20)
    y_axis = np.linspace(-y_range,y_range,20)
    x_mesh, y_mesh = np.meshgrid(x_axis,y_axis)
    
    # store the value of the function in z_mesh in the domain x_mesh and y_mesh
    z_mesh = np.zeros((len(x_axis),len(y_axis)))   
    for i,x in enumerate(x_axis):
        for j,y in enumerate(y_axis):
            z_mesh[i,j] = func(np.array([x,y]))    
            
    # 2D PLOT -----------------------------------------------------------------
    # create figure for 2D visualization
    fig = plt.figure(figsize=(9,7), edgecolor="k")
    ax1 = fig.add_subplot(111)
    
    # plot the iteration steps
    ax1.plot(x_evolution[:,0],x_evolution[:,1], marker='X', color='k')
    
    # plot the color contour of the function
    contour = ax1.contourf(x_mesh, y_mesh, z_mesh, 50, vmin = np.min(z_mesh), vmax = np.max(z_mesh), cmap='seismic', alpha=0.7)
    
    # set axes labels and colorbar
    ax1.set_xlabel("$x_1$", fontdict = {"fontsize": 15})
    ax1.set_ylabel("$x_2$", fontdict = {"fontsize": 15})
    ax1.set_title("2D representation Method: " + method_name, fontdict = {"fontsize": 15}, loc = "center")
    fig.colorbar(contour,  aspect=10)
    
    # save figure
    plt.savefig("optimization_2D_plot_" + method_name + ".png")
    
    # 3D PLOT -----------------------------------------------------------------
    # create figure for 3D visualization
    fig = plt.figure(figsize=(9,7), edgecolor="k")
    ax2 = fig.add_subplot(111, projection='3d')
    
    # plot the iteration steps
    ax2.plot(x_evolution[:,0], x_evolution[:,1], func_value, color='k', marker='X')
    
    # plot the 3D function surface
    surface = ax2.plot_surface(x_mesh, y_mesh, z_mesh, vmin = np.min(z_mesh), vmax = np.max(z_mesh), cmap='seismic', alpha=0.5)
    
    # set axces labels
    ax2.set_xlabel("$x_1$", fontdict = {"fontsize": 15})
    ax2.set_ylabel("$x_2$", fontdict = {"fontsize": 15})
    ax2.set_zlabel('$f(x)$', fontdict = {"fontsize": 15})
    ax2.set_title("3D representation Method: " + method_name, fontdict = {"fontsize": 15}, loc = "center")
    # include colorbar for reference
    fig.colorbar(surface,  aspect=10)
    ax2.elev=50
    
    # save figure
    plt.savefig("optimization_3D_plot_" + method_name + ".png")
    
    # show plots
    plt.show()
    
    print(func_value) 
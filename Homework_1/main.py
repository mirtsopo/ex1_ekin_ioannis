import numpy as np
from scipy.optimize import minimize
import optimizer
from conjugate_gradient import conjgrad

# The following function could be moved to optimizer.py
# Keeping it here, makes the optimizer.py a "library" file 
# where no function is stored, whereas many functions can call it
def calc_func(x, A, B):
    """
    A function to calculate the function: S(x) = x.T*A*x - x.T*b
    ========== Parameters ==========
    A : matrix; represented as list of lists    
    b: vector; represented as list    
    x : vector; represented as list. The vector to calculate the function for.
    ========== Returns ==========
    Scalar
    The value of the function S(x) evaluated at x
    """
    #convert A and B to numpy arrays for convenient manipulation
    Anp = np.array(A)
    Bnp = np.array(B).T  
    #calculate and return the value of the function as a scalar    
    return 0.5*np.dot(np.dot(x.T,Anp),x) - np.dot(x.T,Bnp)

def basic_func(A, B, method, plot_flag):
    """
    Function that gets information regarding the quadratic equation and 
    minimization method and calls relevant functions
    ========== Parameters ==========
    A : a list of lists 
    Describes the 2D matrix A of the quadratic function
    B : a list
    Describes the vector B of the quadratic function
    method : integer
    an integer from the set {0,1,2} that determines the method to be used
    plot_flag : integer
    an integer from the set {0,1} that determines whether to plot {1} or not {0}
    ========== Returns ==========
    None
    """
    # set initial guess
    guess = [-5,5]
    initial_guess = np.array(guess)
    # scipy optimizer
    if method == 0:
        # initialize the optimizer with the initial guess
        optimizer.init(initial_guess)
          
        # call the optimizer
        res = minimize(calc_func, initial_guess, args=(A,B), method='BFGS', tol=1e-6, callback=optimizer.store)
        # plot if plotting is desired
        if plot_flag == 1:
            optimizer.plot_process(lambda x: calc_func(x,A,B), np.array(optimizer.x_progress), 'EX2_scipy_BFGS')
        
                
        # prints a summary of the optimization (success/fail, no of iterations etc.)
        print(res["x"])
        
    elif method == 1:
        # initialize the optimizer with the initial guess
        optimizer.init(initial_guess)
          
        # call the optimizer
        res = minimize(calc_func, initial_guess, args=(A,B), method='CG', tol=1e-6, callback=optimizer.store)
        # plot if plotting is desired
        if plot_flag == 1:
            optimizer.plot_process(lambda x: calc_func(x,A,B), np.array(optimizer.x_progress), 'EX2_scipy_CG')
        
                
        # prints a summary of the optimization (success/fail, no of iterations etc.)
        print(res["x"])
    
    # conjugate gradient optimizer
    elif method == 2:
        # print the minimum solution of x-vector
        x_evolution = conjgrad(A, B, initial_guess)
        # plot if plotting is desired
        if plot_flag == 1:
            optimizer.plot_process(lambda x: calc_func(x,A,B), x_evolution, 'EX2_CG_without_lib' )
        #print the final result of the optimization
        print (x_evolution[-1])
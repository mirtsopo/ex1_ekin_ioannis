Exercise 1 - Scientific Programming for Engineers

--Description

This repository contains the design and implementation of two exercises that focus on Git, Python coding and the use of scipy, numpy, matplotlib and argparse libraries. More specifically, the goal is to minimize a given function, using existing solvers and to implement an iterative solver ourselves, and visualize the results.

--Requirements

The code is tested with the following versions

    Matplotlib 3.2.2
    Python 3.8.3
    Numpy 1.18.5
    Argparse 1.1
    Scipy 1.5.0

--Instructions

To obtain the repository run the code below on terminal: 
git clone git@gitlab.epfl.ch:mirtsopo/ex1_ekin_ioannis.git // clone the repo
The repository should include the files below:
A.csv: File that contains the values of the matrix A in csv. format. The matrix dimensions should be n x n.
B.csv: File that contains the values of the matrix B in csv. format. The matrix dimensions should be n x 1.
conjugate-gradient.py: Students’ implementation of the conjugate gradient method that only uses numpy library.
exercise_1.py: File that solves the quadratic equation described in Exercise 1 of “Week 4 - Homework: Conjugate Gradient”. It requires optimizer.py to function properly.
exercise_2.py: File that makes use of argparse to get the arguments; it returns the minimum values of the chosen solver and optionally plots the solution’s evolution in 2D and 3D, as described in Exercise 2 of “Week 4 - Homework: Conjugate Gradient”. It requires main.py, conjugate_gradient.py and optimizer.py files to function properly.
main.py: File that contains supportive functions for exercise_2.py. It requires conjugate_gradient.py and optimizer.py files to function properly.
optimizer.py: FIle that contains the necessary methods to initialize, store and plot the results of minimization process
README.txt: File that contains the necessary documentation.
.gitignore: File that includes the list of files to be excluded from the version control process
and several .png files produced when the code is run properly.



--File overview
    exercise_1.py : it can be executed either from the terminal or from a Python IDE. Its successful execution plots and saves 4 graphs and prints the summary of the optimization processes. In this case “BFGS” and “CG” solvers from Scipy are used. For both solvers the respective graphs are plotted both in 2D and 3D. The graphs titles as well as the name of the .png files help you to understand which plot refers to which solver.

    exercise_2.py : it has to be executed from the terminal as it requires the user’s input. The user needs to store the matrix A and the vector B of the described quadratic equation in two separate .csv files stored in the same directory. In the terminal the user needs to:
select the solver to minimize the given function, using an integer {0,1,2: 0: Scipy.BFGS, 1: Scipy.CG, 2.CG without libraries}. The Scipy.BFGS will be used by default if no value is given.
choose whether he/she wants to plot the graphs of the optimization evolution, using an integer {0,1, 0: No, 1: Yes}. If no value is given then no plot is expected. Note that plotting option should be chosen only when the problem is 2D, i.e. A is 2 x 2 and B is 2 x 1.
Define the name of the .csv where matrix A is saved, ie.”A.csv”, saved in the same directory.
Define the name of the .csv where vector B is saved, ie.”B.csv”, saved in the same directory.
If additional documentation is needed, the user is invited to type:
python3 exercise_2.py -h

--Authors
Ekin Kubilay
Ioannis Mirtsopoulos
